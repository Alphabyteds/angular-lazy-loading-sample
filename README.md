# angular-lazy-loading-sample
Alphabyte Digital Solutions - Sample angular app for beginners.

Sample application for Angular feature module with lazy loading feature.

Hanlded feature module.

Steps to run
1. Download/Clone repository
2. Navigate respective appliaction folder
3. Run command "npm install"
4. Then run command "ng serve"

Prerequisites:
Node (Latest)
Angular CLI (Latest)

Note: This sample developed with Angular CLI 12.2.14 and npm 8.1.3 and node v16.13.0.
